package com.doniabeje.android.ecommerce


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.runner.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

/**
 * A test using the androidx.test unified API, which can execute on an Android device or locally using Robolectric.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class LoginFragmentTest {
    @Test fun testDisplay() {

        val scenario = launchFragmentInContainer<LoginFragment>()


        onView(withId(R.id.login_TextView))
            .check(matches(isDisplayed()))

        onView(withId(R.id.login_button))
            .check(matches(isDisplayed()))

        onView(withId(R.id.register_button))
            .check(matches(isDisplayed()))

        onView(withId(R.id.password_editText))
            .check(matches(isDisplayed()))

        onView(withId(R.id.email_editText))
            .check(matches(isDisplayed()))
    }

    @Test
    fun testContent(){
        val scenario = launchFragmentInContainer<LoginFragment>()

        onView(withId(R.id.login_TextView))
            .check(matches(withText("LOGIN")))

        onView(withId(R.id.login_button))
            .check(matches(withText("LOGIN")))

        onView(withId(R.id.register_button))
            .check(matches(withText("REGISTER")))

    }


    @Test
    fun testClickRegisterButton() {
        // Given a user in the home screen
        val scenario = launchFragmentInContainer<LoginFragment>()
        val navController = mock(NavController::class.java)

        // Set the NavController property on the fragment
        scenario.onFragment { fragment ->
            Navigation.setViewNavController(fragment.requireView(), navController)
        }

        // When the register button is clicked
        onView(withId(R.id.register_button)).perform(click())
        // Then verify that we navigate to the register screen
        verify(navController).navigate(R.id.action_loginFragment_to_registerFragment)
    }


}
