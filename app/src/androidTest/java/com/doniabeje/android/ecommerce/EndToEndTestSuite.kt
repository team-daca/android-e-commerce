package com.doniabeje.android.ecommerce

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    ProductEndToEndTest::class,
    OrderEndToEndTest::class
)
class EndToEndTestSuite {

}