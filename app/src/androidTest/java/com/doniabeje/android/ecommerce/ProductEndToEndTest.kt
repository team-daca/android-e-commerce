package com.doniabeje.android.ecommerce

import android.graphics.ColorSpace.match
import android.os.SystemClock
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.runner.AndroidJUnit4
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.hamcrest.Matchers.*
import org.junit.BeforeClass

@RunWith(AndroidJUnit4::class)
@LargeTest
class ProductEndToEndTest {



    @Before
    fun initialize() {
        //Start Activity
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

    }

    fun testRetailerLogin() {

        testAddProduct()
        testEditProduct()
        testDeleteProduct()


    }


    fun testCustomerLogin() {

        //enter email
        onView(withId(R.id.email_editText)).perform(typeText("c"))

        //enter password
        onView(withId(R.id.password_editText)).perform(typeText("c"), closeSoftKeyboard())

        //click login button
        onView(withId(R.id.login_button)).perform(click())

        //wait
        SystemClock.sleep(2000)

        //check if feed fragment is opened
        onView(withId(R.id.feed_list)).check(matches(isDisplayed()))

        //logout
        onView(withId(R.id.feedNavLogoutButton)).perform(click())

    }


    fun testRegister() {

        //click login register
        onView(withId(R.id.register_button)).perform(click())

        //fill email
        onView(withId(R.id.email_editText)).perform(typeText("anteneh@gmail.com"))
        //fill password
        onView(withId(R.id.password_editText)).perform(typeText("antenehadmasu"), ViewActions.closeSoftKeyboard())
        //select retailer radiobutton
        onView(withId(R.id.role_retailer_radio)).perform(click())

        //check if Retailer radio is checked
        onView(withId(R.id.role_retailer_radio)).check(matches(isChecked()))

        //click register button
        onView(withId(R.id.register_button_RegisterFragment)).perform(click())

        //check if status registered
        //onView(withId(R.id.errorMessage_textView)).check(matches(withText("Loading...")))

    }

    @Test
    fun testAddProduct() {

        loginRetailer()

        //click add product button(navigate to add product screen)
        onView(withId(R.id.navOrdrsButton)).perform(click())

        //fill product name
        onView(withId(R.id.productNameEditText)).perform(typeText("Nice"), closeSoftKeyboard())

        //fill product price
        onView(withId(R.id.productPriceEditText)).perform(replaceText("400"), closeSoftKeyboard())

        //click add product button
        onView(withId(R.id.addProductButton)).perform(click())
        pressBack()
        //wait
        SystemClock.sleep(1500)

        onView(withText("Nice")).check(matches(isDisplayed()))

    }


    @Test
    fun testEditProduct() {

        onView(
            allOf(
                withParent(withChild(withChild(withText("Nice")))),
                withId(R.id.edit_order_button_my_product)
            )
        ).perform(click())
        onView(withId(R.id.productNameEditText)).perform(replaceText("Nice Edited"), closeSoftKeyboard())
        onView(withText("OK")).perform(click())

    }


    @Test
    fun testDeleteProduct() {

        onView(allOf(withParent(withChild(withChild(withText("Nice Edited")))), withId(R.id.delete_Product_button))).perform(
            click()
        )
        //wait
        SystemClock.sleep(500);
        onView(withText("Nice Edited")).check(doesNotExist())

        //logout
        onView(withId(R.id.navLogoutButton)).perform(click())

    }

    fun loginRetailer() {

        //enter email
        onView(withId(R.id.email_editText)).perform(typeText("doni@gmail.com"), closeSoftKeyboard())

        //enter password
        onView(withId(R.id.password_editText)).perform(typeText("doniabeje"), closeSoftKeyboard())

        //click login button
        onView(withId(R.id.login_button)).perform(click())

        //wait
        SystemClock.sleep(1500);
    }


    fun testAddOrder() {
        //Start Activity
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

        //enter email
        onView(withId(R.id.email_editText)).perform(typeText("c"))

        //enter password
        onView(withId(R.id.password_editText)).perform(typeText("c"), closeSoftKeyboard())

        //click login button
        onView(withId(R.id.login_button)).perform(click())

        //wait
        SystemClock.sleep(1500)

        //click product cardview
        onView(allOf(withText("sdf"), withId(R.id.myProductName), withParentIndex(0))).perform(click())

        //fill product amount
        onView(withId(R.id.singleOrderAmountEditText)).perform(typeText("4"), closeSoftKeyboard())


        //click add order button
        onView(withId(R.id.singleAddOrderButton)).perform(click())
        pressBack()
        //wait
        SystemClock.sleep(1500)
        onView(withId(R.id.feedNavLogoutButton)).perform(click())

    }
}