package com.doniabeje.android.ecommerce


import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.runner.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

/*
 * A test using the androidx.test unified API, which can execute on an Android device or locally using Robolectric.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

 //RegisterFragment.kt
@RunWith(AndroidJUnit4::class)
class RegisterFragmentTest {
    @Test fun testDisplay() {

        val scenario = launchFragmentInContainer<RegisterFragment>()

        onView(withId(R.id.register_TextView))
            .check(matches(isDisplayed()))

        onView(withId(R.id.email_editText))
            .check(matches(isDisplayed()))

        onView(withId(R.id.password_editText))
            .check(matches(isDisplayed()))

        onView(withId(R.id.role_radio))
            .check(matches(isDisplayed()))

        onView(withId(R.id.register_button_RegisterFragment))
            .check(matches(isDisplayed()))
    }

    @Test
    fun testContent(){
        val scenario = launchFragmentInContainer<RegisterFragment>()

        onView(withId(R.id.register_TextView))
            .check(matches(withText("REGISTER")))

        onView(withId(R.id.register_button_RegisterFragment))
            .check(matches(withText("REGISTER")))

        onView(withId(R.id.role_retailer_radio))
            .check(matches(isChecked()))

    }


}
