package com.doniabeje.android.ecommerce


import android.app.Application
import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.runner.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

/*
 * A test using the androidx.test unified API, which can execute on an Android device or locally using Robolectric.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
 
 //AddProductFragmentTest.kt
@RunWith(AndroidJUnit4::class)
class AddProductFragmentTest {
    @Test fun testDisplay() {

        val scenario = launchFragmentInContainer<AddProductFragment>()

        onView(withId(R.id.add_product_textView))
            .check(matches(isDisplayed()))

        onView(withId(R.id.productNameEditText))
            .check(matches(isDisplayed()))

        onView(withId(R.id.productPriceEditText))
            .check(matches(isDisplayed()))

        onView(withId(R.id.addProductButton))
            .check(matches(isDisplayed()))

    }

    @Test
    fun testContent(){
        val scenario = launchFragmentInContainer<AddProductFragment>()
        val context = getApplicationContext<Application>()

        onView(withId(R.id.add_product_textView))
            .check(matches(withText(context.resources.getString(R.string.add_product))))

        onView(withId(R.id.addProductButton))
            .check(matches(withText(context.resources.getString(R.string.add_product))))


    }


}
