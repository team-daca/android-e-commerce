package com.doniabeje.android.ecommerce

import android.graphics.ColorSpace.match
import android.os.SystemClock
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.runner.AndroidJUnit4
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.hamcrest.Matchers.*
import org.junit.BeforeClass

@RunWith(AndroidJUnit4::class)
@LargeTest
class OrderEndToEndTest {


    @Before
    fun initialize() {
        //Start Activity
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

    }




    @Test
    fun testAdd() {

        loginCustomer()

        onView(
            allOf(withId(R.id.myProductName), withText("IPhone XII"))

        ).perform(
            click()
        )

        onView(withHint("Amount")).perform(typeText("10"), closeSoftKeyboard())

        onView(withText("ORDER")).perform(click())
        pressBack()
        //wait
        SystemClock.sleep(500);
        onView(withText("ORDERS")).perform(click())
        SystemClock.sleep(500);
        onView(withText("IPhone XII")).check(matches(isDisplayed()))
    }

    @Test
    fun testEdit() {
        onView(withText("ORDERS")).perform(click())
        onView(
            allOf(
                withParent(withChild(withChild(withText("IPhone XII")))),
                withId(R.id.edit_order_button)
            )
        ).perform(click())
        onView(withId(R.id.edit_ordere_amount_edit_text)).perform(replaceText("30"), closeSoftKeyboard())
        onView(withText("OK")).perform(click())
        SystemClock.sleep(1000)
    }


    @Test
    fun testDelete() {
        onView(withText("ORDERS")).perform(click())
        onView(
            allOf(
                withParent(withChild(withChild(withText("IPhone XII")))),
                withId(R.id.delete_order_button)
            )
        ).perform(
            click()
        )
        //wait
        SystemClock.sleep(1000);
        onView(withText("IPhone XII")).check(doesNotExist())
        pressBack()
        //logout
        SystemClock.sleep(500);
        onView(withId(R.id.feedNavLogoutButton)).perform(click())

    }


    fun loginCustomer() {

        //enter email
        onView(withId(R.id.email_editText)).perform(typeText("abel@gmail.com"), closeSoftKeyboard())

        //enter password
        onView(withId(R.id.password_editText)).perform(typeText("abelabel"), closeSoftKeyboard())

        //click login button
        onView(withId(R.id.login_button)).perform(click())

        //wait
        SystemClock.sleep(1500);

    }
}