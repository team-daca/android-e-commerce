package com.doniabeje.android.ecommerce

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    AddProductFragmentTest::class,
    FeedFragmentTest::class,
    RegisterFragmentTest::class,
    MyProductFragmentTest::class
)
class IntegrationTestSuite {

}