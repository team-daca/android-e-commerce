package com.doniabeje.android.ecommerce


import android.app.Application
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.runner.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

/**
 * A test using the androidx.test unified API, which can execute on an Android device or locally using Robolectric.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
    class FeedFragmentTest {
    @Test
    fun testDisplay() {

        val scenario = launchFragmentInContainer<FeedFragment>()

        onView(withId(R.id.feedNavOrdersButton))
            .check(matches(isDisplayed()))

        onView(withId(R.id.feedNavLogoutButton))
            .check(matches(isDisplayed()))

        onView(withId(R.id.feed_list))
            .check(matches(isDisplayed()))


    }

    @Test
    fun testContent() {
        val scenario = launchFragmentInContainer<FeedFragment>()
        val context = getApplicationContext<Application>()

        onView(withId(R.id.feedNavOrdersButton))
            .check(matches(withText(context.resources.getString(R.string.orders))))

        onView(withId(R.id.feedNavLogoutButton))
            .check(matches(withText("LOGOUT")))


    }


    @Test
    fun testClickOrderButton() {
        // Given a user in the home screen
        val scenario = launchFragmentInContainer<FeedFragment>()
        val navController = mock(NavController::class.java)

        scenario.onFragment {
            Navigation.setViewNavController(it.view!!, navController)
        }

        // When the orders button is clicked
        onView(withId(R.id.feedNavOrdersButton)).perform(click())

        // Then verify that we navigate to the orders screen
        verify(navController).navigate(R.id.action_feedFragment_to_ordersFragment)
    }
    @Test

    fun testClickLogoutButton() {
        // Given a user in the home screen
        val scenario = launchFragmentInContainer<FeedFragment>()
        val navController = mock(NavController::class.java)

        scenario.onFragment {
            Navigation.setViewNavController(it.view!!, navController)
        }

        // When the orders button is clicked
        onView(withId(R.id.feedNavLogoutButton)).perform(click())

        // Then verify that we navigate to the orders screen
        verify(navController).popBackStack()
    }
}
