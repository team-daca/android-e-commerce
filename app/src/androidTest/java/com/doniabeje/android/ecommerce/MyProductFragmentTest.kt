package com.doniabeje.android.ecommerce


import android.app.Application
import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.runner.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito

/**
 * A test using the androidx.test unified API, which can execute on an Android device or locally using Robolectric.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class MyProductFragmentTest {

    @Test fun testDisplay() {

        val scenario = launchFragmentInContainer<MyProductFragment>()

        onView(withId(R.id.navAddProductButton))
            .check(matches(isDisplayed()))

        onView(withId(R.id.navOrdrsButton))
            .check(matches(isDisplayed()))

        onView(withId(R.id.navLogoutButton))
            .check(matches(isDisplayed()))


    }

    @Test
    fun testContent(){
        val scenario = launchFragmentInContainer<MyProductFragment>()
        val context = getApplicationContext<Application>()

        onView(withId(R.id.navAddProductButton))
            .check(matches(withText(context.resources.getString(R.string.orders))))




    }

    @Test

    fun testClickOrdersButton() {
        // Given a user in the home screen
        val scenario = launchFragmentInContainer<MyProductFragment>()
        val navController = Mockito.mock(NavController::class.java)

        scenario.onFragment {
            Navigation.setViewNavController(it.view!!, navController)
        }

        // When the orders button is clicked
        onView(withId(R.id.navAddProductButton)).perform(click())

        // Then verify that we navigate to the orders screen
        Mockito.verify(navController).navigate(R.id.action_productFragment_to_ordersFragment)
    }

    fun testClickAddProductButton() {
        // Given a user in the home screen
        val scenario = launchFragmentInContainer<MyProductFragment>()
        val navController = Mockito.mock(NavController::class.java)

        scenario.onFragment {
            Navigation.setViewNavController(it.view!!, navController)
        }

        // When the orders button is clicked
        onView(withId(R.id.navOrdrsButton)).perform(click())

        // Then verify that we navigate to the orders screen
        Mockito.verify(navController).navigate(R.id.action_productFragment_to_addProductFragment)
    }

}
