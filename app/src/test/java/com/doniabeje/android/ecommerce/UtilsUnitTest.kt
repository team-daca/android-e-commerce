package com.doniabeje.android.ecommerce

import org.junit.Test
class UtilsUnitTest {

    @Test
    fun testPasswordStrength(){
        val incorrectPassword = "pass"
        val incorrectResult = Utils.checkPasswordStrength(incorrectPassword)
        val correctPassword = "hit56"
        val correctResult = Utils.checkPasswordStrength(correctPassword)

        assert(!incorrectResult)
        assert(correctResult)
    }

    @Test
    fun testUserName(){
        val correctUserName = "jovani"
        val correctResult = Utils.checkUserName(correctUserName)
        val incorrectUserName = "doni"
        val incorrectResult = Utils.checkUserName(incorrectUserName)
        assert(!incorrectResult)
        assert(correctResult)
    }

    @Test
    fun testProductName(){
        val incorrectProductName = "1"
        val incorrectProductName2 = "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"

        val incorrectResult = Utils.checkProductName(incorrectProductName)
        val incorrectResult2 = Utils.checkProductName(incorrectProductName2)

        assert(!incorrectResult)
        assert(!incorrectResult2)
    }
}