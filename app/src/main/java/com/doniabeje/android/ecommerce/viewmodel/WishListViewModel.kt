package com.doniabeje.android.ecommerce.viewmodel

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.doniabeje.android.ecommerce.OrdersRecyclerViewAdapter
import com.doniabeje.android.ecommerce.WishListRecyclerViewAdapter
import com.doniabeje.android.ecommerce.data.Order
import com.doniabeje.android.ecommerce.data.Product
import com.doniabeje.android.ecommerce.data.User
import com.doniabeje.android.ecommerce.data.WishList
import com.doniabeje.android.ecommerce.network.APIService
import com.doniabeje.android.ecommerce.repository.EcommerceRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WishListViewModel(application: Application) : BaseViewModel(application),WishListRecyclerViewAdapter.OnDeleteWishListListener {
    override fun deleteWishList(id: Int) {
        if (!repository.isConnected()){
            repository.notConnected()
            return
        }

        viewModelScope.launch {
            val deleted = repository.deleteWishList(id)
            viewModelScope.launch(Dispatchers.Main) {
                if (deleted) {
                    Toast.makeText(getApplication(), "Wish List Deleted", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(getApplication(), "Unable To Deleted Wish List ", Toast.LENGTH_SHORT).show()

                }
            }

        }
    }

    lateinit var wishList: LiveData<List<WishList>>

    init {
        val userId = User.loggedInUser?.id
        userId?.let {
             wishList = repository.getCustomerWishLists(it)

        }

    }


}