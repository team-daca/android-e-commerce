package com.doniabeje.android.ecommerce

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.doniabeje.android.ecommerce.data.Product
import com.doniabeje.android.ecommerce.data.WishList
import com.doniabeje.android.ecommerce.databinding.FragmentFeedBinding
import com.doniabeje.android.ecommerce.viewmodel.FeedViewModel
import com.doniabeje.android.ecommerce.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.fragment_feed.view.*

class FeedFragment : Fragment(), FeedRecyclerViewAdapter.OnWishListToggleListener {
    override fun toggle(wishList: WishList?, product: Product) {
        if (wishList != null){
            //remove wish list
            viewModel.deleteWishList(wishList.id)
        }
        else{
            //add wish list
            viewModel.addWishList(product)
        }
    }

    lateinit var viewModel: FeedViewModel
    lateinit var recyclerViewAdapter: FeedRecyclerViewAdapter
    lateinit var binding: FragmentFeedBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(FeedViewModel::class.java)
        viewModel.sharedPreferences = context?.getSharedPreferences("USERDATA", Context.MODE_PRIVATE)
        recyclerViewAdapter = FeedRecyclerViewAdapter(listOf(),listOf(),this)

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_feed, container, false)
        binding.viewModel = viewModel
        binding.feedList.layoutManager = LinearLayoutManager(context)
        binding.feedList.adapter = recyclerViewAdapter
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.products.observe(viewLifecycleOwner, Observer { products ->
            recyclerViewAdapter.updateProductWithReviewss(products)
        })
        arguments?.let {
            val customerId = it.getInt("CustomerId")
            viewModel.getCustomerWishLists(customerId)

        }
        viewModel.wishList.observe(
            viewLifecycleOwner,
            Observer {
                    wishList -> recyclerViewAdapter.updateWishList(wishList)

            })


    }


}
