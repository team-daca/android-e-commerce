package com.doniabeje.android.ecommerce

import android.app.AlertDialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.doniabeje.android.ecommerce.data.Order
import com.doniabeje.android.ecommerce.data.Product
import com.doniabeje.android.ecommerce.data.User
import com.doniabeje.android.ecommerce.databinding.FragmentOrdersBinding
import com.doniabeje.android.ecommerce.databinding.FragmentWishListBinding
import com.doniabeje.android.ecommerce.viewmodel.OrdersViewModel
import com.doniabeje.android.ecommerce.viewmodel.WishListViewModel
import kotlinx.android.synthetic.main.edit_order.view.*
import kotlinx.android.synthetic.main.edit_product.view.*
import kotlinx.android.synthetic.main.fragment_wish_list.view.*


class WishListFragment : Fragment() {


    lateinit var wishListRecyclerViewAdapter: WishListRecyclerViewAdapter
    lateinit var viewModel: WishListViewModel
    lateinit var binding: FragmentWishListBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(WishListViewModel::class.java)
        wishListRecyclerViewAdapter = WishListRecyclerViewAdapter(listOf(),  viewModel)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wish_list, container, false)
        with(binding.wishListRecyclerView) {
            adapter = wishListRecyclerViewAdapter
            layoutManager = LinearLayoutManager(context)

        }

        viewModel.wishList.observe(
            viewLifecycleOwner,
            Observer { wishList ->
                wishListRecyclerViewAdapter.updateWishList(wishList)
            })

        return binding.root
    }



}
