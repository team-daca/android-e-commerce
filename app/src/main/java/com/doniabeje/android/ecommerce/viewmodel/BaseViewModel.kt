package com.doniabeje.android.ecommerce.viewmodel

import android.app.AlertDialog
import android.app.Application
import android.view.LayoutInflater
import androidx.databinding.BaseObservable
import android.view.View
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.NavController
import com.doniabeje.android.ecommerce.MainActivity
import com.doniabeje.android.ecommerce.R
import com.doniabeje.android.ecommerce.data.EcommerceDao
import com.doniabeje.android.ecommerce.data.EcommerceDatabase
import com.doniabeje.android.ecommerce.network.APIService
import com.doniabeje.android.ecommerce.repository.EcommerceRepository

open class BaseViewModel(application: Application) : AndroidViewModel(application) {
    val apiService: APIService = APIService.getInstance()
    val dao: EcommerceDao = EcommerceDatabase.getDatabase(application).ecommerceDao()
    val repository: EcommerceRepository = EcommerceRepository(application, apiService, dao)
    var navController: NavController? = null


}