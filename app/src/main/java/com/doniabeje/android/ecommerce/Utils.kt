package com.doniabeje.android.ecommerce

//Utils.kt
class Utils {

    companion object {

        fun checkPasswordStrength(password: String): Boolean {
            val hasNumber = password.contains(Regex("[0-9]"))
            val hasAlpha = password.contains(Regex("[a-zA-Z]"))
            return password.length > 4 && hasNumber && hasAlpha
        }

        fun checkUserName(userName: String): Boolean {
            return userName.length > 4
        }

        fun checkProductName(name: String): Boolean {
            return name.length < 20 && name.contains(Regex("[a-zA-Z]"))
        }
    }
}
