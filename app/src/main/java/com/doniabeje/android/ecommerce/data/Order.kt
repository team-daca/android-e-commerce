package com.doniabeje.android.ecommerce.data

import androidx.room.*

//Order.kt
@Entity
data class Order(
    @PrimaryKey val id: Int = 0,
    @Embedded(prefix = "product_")
    val product: Product? = null,
    val productId: Int,
    @Ignore
    val customer: Customer? = null,
    val customerId: Int,
    val retailerId: Int,
    @Ignore
    val retailer: Retailer? = null,
    var amount: Int
) {
    constructor(
        id: Int = 0,
        product: Product? = null,
        productId: Int,
        customerId: Int,
        retailerId: Int,
        amount: Int
    )
            : this(id, product, productId, null, customerId, retailerId, null, amount)

}