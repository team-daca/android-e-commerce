package com.doniabeje.android.ecommerce.network

import com.doniabeje.android.ecommerce.data.*
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

//APIService.kt
interface APIService {

    @POST("auth/login")
    fun login(@Body newUser: User): Deferred<Response<User>>

    @GET("retailers/{id}")
    fun getRetailer(@Path("id") id: Int?): Deferred<Response<Retailer>>

    @GET("customers/{id}")
    fun getCustomer(@Path("id") id: Int?): Deferred<Response<Customer>>

    @GET("products")
    fun getProducts(): Deferred<Response<List<Product>>>

    @POST("products")
    fun addProduct(@Body product: Product?): Deferred<Response<Product>>

    @DELETE("products/{id}")
    fun deleteProduct(@Path("id") id:Int): Deferred<Response<Void>>

    @PUT("products/{id}")
    fun updateProduct(@Path("id") id:Int, @Body product: Product): Deferred<Response<Void>>

    @POST("orders")
    fun addOrder(@Body order: Order): Deferred<Response<Order>>

    @PUT("orders/{id}")
    fun updateOrder(@Path("id") id:Int, @Body order: Order): Deferred<Response<Void>>

    @DELETE("orders/{id}")
    fun deleteOrder(@Path("id") id:Int): Deferred<Response<Void>>

    @POST("auth/register")
    fun addUser(@Body user: User): Deferred<Response<User>>

    @GET("products/retailer/{id}")
    fun getRetailerProducts(@Path("id") id: Int): Deferred<Response<List<Product>>>

    @GET("orders/retailer/{id}")
    fun getRetailerOrders(@Path("id") id: Int): Deferred<Response<List<Order>>>

    @GET("orders/customer/{id}")
    fun getCustomerOrders(@Path("id") id: Int): Deferred<Response<List<Order>>>

    @GET("products/{id}")
    fun getProduct(@Path("id") id: Int): Deferred<Response<Product>>

    //WishList

    @GET("wishlists/customer/{id}")
    fun getCustomerWishLists(@Path("id") id: Int): Deferred<Response<List<WishList>>>

    @POST("wishlists")
    fun addWishList(@Body wishList: WishList): Deferred<Response<WishList>>

    @DELETE("wishlists/{id}")
    fun deleteWishList(@Path("id") id:Int): Deferred<Response<Void>>

    @PUT("wishlists/{id}")
    fun updateProduct(@Path("id") id:Int, @Body wishList: WishList): Deferred<Response<Void>>


    //Product Review

    @POST("ProductReviews")
    fun addProductReview(@Body productReview: ProductReview): Deferred<Response<ProductReview>>

    @DELETE("ProductReviews/{id}")
    fun deleteProductReview(@Path("id") id:Int): Deferred<Response<Void>>

    @PUT("ProductReviews/{id}")
    fun updateProductReviews(@Path("id") id:Int, @Body productReview: ProductReview): Deferred<Response<Void>>


    companion object {

        private val baseUrl = "http://192.168.43.184:5002/api/"

        fun getInstance(): APIService {

            val client = OkHttpClient
                .Builder()
                .build()

            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(MoshiConverterFactory.create())
                .build()

            return retrofit.create(APIService::class.java)
        }
    }
}
