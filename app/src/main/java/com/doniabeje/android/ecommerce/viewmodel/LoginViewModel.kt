package com.doniabeje.android.ecommerce.viewmodel

import android.app.Application
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.navOptions
import com.doniabeje.android.ecommerce.R
import com.doniabeje.android.ecommerce.data.User
import com.doniabeje.android.ecommerce.data.UserRole
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

const val USERID = "USERID"
const val USERROLE = "USERROLE"

class LoginViewModel(application: Application) : BaseViewModel(application) {

    var errorMessage = MutableLiveData<String>("")
    var email = ObservableField<String>("")
    var password = ObservableField<String>("")
    var sharedPreferences: SharedPreferences? = null

    fun login(view: View) {

        if (!repository.isConnected()) {
            repository.notConnected()
            return
        }

        val user = User(email = email.get(), password = password.get(), userRoleId = -1)

        errorMessage.postValue("Loading...")
        var response: Response<User>
        viewModelScope.launch(Dispatchers.IO) {
            response = repository.login(user).await()

            when {
                response.isSuccessful -> {

                    errorMessage.postValue("Success")
                    saveUser(response.body())
                    loggedIn(response.body(), navController)
                }

                response.code() == 401 ->
                    errorMessage.postValue("Incorrect Login Information")

                else -> {
                    Toast.makeText(getApplication(), "Network Error On Login", Toast.LENGTH_SHORT).show()
                    errorMessage.postValue("")
                }
            }
        }


    }

    private fun loggedIn(user: User?, navigation: NavController?) {

        User.loggedInUser = user
        val options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }
        when (user?.role?.name) {

            "CUSTOMER" -> {
                //Todo Add Customer To Database
                val userId = user?.id
                val arg = Bundle()
                arg.putInt("CustomerId", userId)
                navController?.navigate(R.id.action_loginFragment_to_feedFragment, arg, options)
            }


            "RETAILER" -> {
                //Todo  Add Retailer To Database
                val userId = user?.id
                val arg = Bundle()
                arg.putInt("RetailerId", userId)
                navController?.navigate(R.id.action_loginFragment_to_productFragment, arg, options)
            }

        }
    }

    private fun saveUser(user: User?) {
        user?.let {
            sharedPreferences?.let {
                with(it.edit()) {
                    putInt(USERID, user.id)
                    putString(USERROLE, user.role?.name)
                    commit()
                }
            }
        }
    }

    fun logInOffline() {
        sharedPreferences?.let {

            val userId = it.getInt(USERID, 0)
            val userRoleName = it.getString(USERROLE, "")
            val userRole = UserRole(name = userRoleName)
            if (userId != 0 && userRoleName.isNotEmpty()) {
                User.loggedInUser = User(id = userId, email = null, password = null, userRoleId = 0, role = userRole)
                loggedIn(User.loggedInUser, navController)
            }
        }
    }

    fun navigateToRegister(view: View) {
        navController?.navigate(R.id.action_loginFragment_to_registerFragment)
    }

}
