package com.doniabeje.android.ecommerce

import android.app.AlertDialog
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.doniabeje.android.ecommerce.data.Product
import com.doniabeje.android.ecommerce.databinding.FragmentProductListBinding
import com.doniabeje.android.ecommerce.viewmodel.MyProductsViewModel
import kotlinx.android.synthetic.main.edit_product.view.*
import kotlinx.android.synthetic.main.fragment_product_list.view.*


class MyProductFragment : Fragment(), MyProductRecyclerViewAdapter.OnEditProductListener {
    override fun editProduct(product: Product) {
        showEditProductDialog(product)
    }

    lateinit var recyclerViewAdapter: MyProductRecyclerViewAdapter
    lateinit var viewModel: MyProductsViewModel
    lateinit var binding: FragmentProductListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MyProductsViewModel::class.java)
        recyclerViewAdapter = MyProductRecyclerViewAdapter(listOf(), viewModel, this)
        viewModel.sharedPreferences = context?.getSharedPreferences("USERDATA", Context.MODE_PRIVATE)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_list, container, false)
        binding.viewModel = viewModel

        // Set the adapter
        val recyclerView = binding.myProductsRecyclerView

        recyclerView.layoutManager = GridLayoutManager(context, 2)
        recyclerView.adapter = recyclerViewAdapter


        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        arguments?.let {
            val retailerId = it.getInt("RetailerId")
            viewModel.getRetailerProducts(retailerId)
            viewModel.products.observe(
                viewLifecycleOwner,
                Observer { products -> recyclerViewAdapter.updateProducts(products) })
        }
    }

    fun showEditProductDialog(product: Product) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Edit Product")
        val editView = LayoutInflater.from(context).inflate(R.layout.edit_product, null)
        editView.productNameEditText.setText(product.name)
        editView.productPriceEditText.setText(product.price.toString())
        builder.setView(editView)
        builder.setNegativeButton(android.R.string.cancel) { dialog, p1 ->
            dialog.cancel()
        }
        builder.setPositiveButton(android.R.string.ok) { dialog, p1 ->
            viewModel.updateProduct(
                product,
                editView.productNameEditText.text.toString(),
                editView.productPriceEditText.text.toString().toFloat()
            )
        }
        builder.show()
    }

}




