package com.doniabeje.android.ecommerce

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.doniabeje.android.ecommerce.data.User
import com.doniabeje.android.ecommerce.databinding.SingleProductFragmentBinding
import com.doniabeje.android.ecommerce.viewmodel.SingleProductViewModel
import java.lang.Exception

//SingleProductFragment.kt
class SingleProductFragment : Fragment() {

    private lateinit var viewModel: SingleProductViewModel
    private lateinit var binding: SingleProductFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.single_product_fragment, container, false)
        viewModel = ViewModelProviders.of(this).get(SingleProductViewModel::class.java)
        arguments?.let {
            val id = it.getInt("ProductId")
            viewModel.getProductWithReview(id)

//            viewModel.product.observe(viewLifecycleOwner,
//                Observer { product ->
//                    run {
//                        binding.singleProductNameTextView.text = product.name
//                        binding.singleProductPriceTextView.text = "$${product.price}"
//                    }
//                })


            viewModel.productWithReview.observe(viewLifecycleOwner,
                Observer { productWithReview ->
                    run {
                        binding.singleProductNameTextView.text = productWithReview.product.name
                        binding.singleProductPriceTextView.text = "$${productWithReview.product.price}"


                        User.loggedInUser?.let {
                            val review = productWithReview.reviews.find { r -> r.userId == it.id}
                            review?.let {
                                binding.ratingBar2.rating = review.rating.toFloat()
                            }
                        }



                    }
                })
        }

        binding.singleAddOrderButton.setOnClickListener {
            val amount = binding.singleOrderAmountEditText.text.toString().toInt()
            viewModel.addOrder(amount)
        }
        binding.ratingBar2.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->

            viewModel.addOrUpdateRating(rating)}
        return binding.root
    }


}
