package com.doniabeje.android.ecommerce.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

//EcommerceDao.kt
@Dao
interface EcommerceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProduct(vararg product: Product)

    @Query(" SELECT * FROM PRODUCT WHERE id = :id")
    fun getProductWithReview(id: Int): ProductWithReview

    @Query(" SELECT * FROM PRODUCT WHERE id = :id")
    fun getProduct(id: Int): Product

    @Query(" SELECT * FROM PRODUCT ")
    fun getProductWithReviews(): LiveData<List<ProductWithReview>>

    @Query(" SELECT * FROM PRODUCT ")
    fun getAllProducts(): LiveData<List<Product>>

    @Query(" DELETE  FROM PRODUCT ")
    fun deleteAllProduct()

    @Query(" DELETE  FROM PRODUCT WHERE id = :id ")
    fun deleteProduct(id: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrder(vararg order: Order)

    @Query(" SELECT * FROM `Order` WHERE id = :id")
    fun getOrder(id: Int): Order

    @Query(" SELECT * FROM `Order` ")
    fun getAllOrder(): List<Order>

    @Query(" SELECT * FROM `Order` ")
    fun getAllOrders(): LiveData<List<Order>>

    @Query(" DELETE  FROM `Order` ")
    fun deleteAllOrder()

    @Query(" DELETE  FROM `Order` WHERE id = :id")
    fun deleteOrder(id: Int)

    @Query(" DELETE  FROM `Order` WHERE productId = :id")
    fun deleteOrderByProductID(id: Int)

    //WishList

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWishList(vararg wishList: WishList)

    @Query(" SELECT * FROM WishList ")
    fun getAllWishLists(): LiveData<List<WishList>>

    @Query(" DELETE  FROM WishList ")
    fun deleteAllWishList()

    @Query(" DELETE  FROM WishList WHERE id = :id ")
    fun deleteWishList(id: Int)

    @Query(" DELETE  FROM WishList WHERE productId = :id")
    fun deleteWishListByProductID(id: Int)


    //Product Review

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProductReview(vararg productReview: ProductReview)


    @Query(" DELETE  FROM ProductReview ")
    fun deleteAllProductReview()

    @Query(" DELETE  FROM ProductReview WHERE id = :id ")
    fun deleteProductReview(id: Int)

    @Query(" DELETE  FROM ProductReview WHERE productId = :id")
    fun deleteProductReviewByProductID(id: Int)

}