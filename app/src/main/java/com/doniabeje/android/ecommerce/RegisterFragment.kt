package com.doniabeje.android.ecommerce

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.doniabeje.android.ecommerce.data.User
import com.doniabeje.android.ecommerce.databinding.LoginFragmentBinding
import com.doniabeje.android.ecommerce.databinding.RegisterFragmentBinding
import com.doniabeje.android.ecommerce.viewmodel.LoginViewModel
import com.doniabeje.android.ecommerce.viewmodel.RegisterViewModel
import kotlinx.android.synthetic.main.login_fragment.*
import kotlinx.android.synthetic.main.login_fragment.view.*

//RegisterFragment.kt
class RegisterFragment : Fragment() {
    lateinit var binding: RegisterFragmentBinding
    private lateinit var viewModel: RegisterViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.register_fragment, container, false)
        viewModel = ViewModelProviders.of(this).get(RegisterViewModel::class.java)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

}
