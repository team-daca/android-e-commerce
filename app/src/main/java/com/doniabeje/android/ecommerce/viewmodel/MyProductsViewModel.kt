package com.doniabeje.android.ecommerce.viewmodel

import android.app.Application
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.Navigation
import com.doniabeje.android.ecommerce.MyProductRecyclerViewAdapter
import com.doniabeje.android.ecommerce.R
import com.doniabeje.android.ecommerce.data.Product
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MyProductsViewModel(application: Application) : BaseViewModel(application),
    MyProductRecyclerViewAdapter.OnDeleteProductListener {


    override fun deleteProduct(id: Int) {
        if (!repository.isConnected()){
            repository.notConnected()
            return
        }
        viewModelScope.launch {
            val deleted = repository.deleteProduct(id)
            viewModelScope.launch(Dispatchers.Main) {
                if (deleted) {
                    Toast.makeText(getApplication(), "Product Deleted", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(getApplication(), "Unable To Deleted Product ", Toast.LENGTH_SHORT).show()

                }
            }

        }
    }

    var products: LiveData<List<Product>> = MutableLiveData<List<Product>>(listOf())
    var sharedPreferences: SharedPreferences? = null


    fun logout(view: View) {
        sharedPreferences?.let {
            with(it.edit()) {
                remove(com.doniabeje.android.ecommerce.viewmodel.USERID)
                putString(com.doniabeje.android.ecommerce.viewmodel.USERROLE, "")
                commit()
            }
        }

        repository.clearDatabase()
        Navigation.findNavController(view)
            .popBackStack()
    }

    fun getRetailerProducts(retailerId: Int) {
        products = repository.getRetailerProducts(retailerId)
    }


    fun navToOrdersFragment(view: View) {
        Navigation.findNavController(view).navigate(R.id.action_productFragment_to_addProductFragment)
    }

    fun navToAddProductFragment(view: View) {
        Navigation.findNavController(view).navigate(R.id.action_productFragment_to_ordersFragment)
    }

    fun updateProduct(product: Product, name:String, price:Float){
        if (!repository.isConnected()){
            repository.notConnected()
            return
        }
        viewModelScope.launch {
            product.name = name
            product.price = price
            val updated = repository.updateProduct(product)

                viewModelScope.launch(Dispatchers.Main){
                    if (updated){
                    Toast.makeText(getApplication(),"Product Updated", Toast.LENGTH_SHORT).show()
                }
                    else{
                        Toast.makeText(getApplication(),"Unable To Updated Product", Toast.LENGTH_SHORT).show()
                    }
            }
        }
    }


}
