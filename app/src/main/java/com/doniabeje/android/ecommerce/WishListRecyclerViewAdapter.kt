package com.doniabeje.android.ecommerce

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.doniabeje.android.ecommerce.data.WishList


import kotlinx.android.synthetic.main.fragment_wish_list_card.view.*

class WishListRecyclerViewAdapter(
    private var wishList: List<WishList>,
    private var deleteWishListListener: OnDeleteWishListListener
) : RecyclerView.Adapter<WishListRecyclerViewAdapter.ViewHolder>() {


    fun updateWishList(wishList : List<WishList>) {
        this.wishList = wishList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_wish_list_card, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val wishList = wishList[position]
        val price = wishList.product?.price.toString()
        val name = wishList.product?.name

        with(holder) {
            mName.text = name
            mPriceView.text = "$$price"
            mDeleteWishListButton.setOnClickListener { deleteWishListListener.deleteWishList(wishList.id) }
        }

    }

    override fun getItemCount(): Int = wishList.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mName: TextView = mView.wishListProductName
        val mPriceView: TextView = mView.wishListProductPrice
        val mDeleteWishListButton:ImageButton = mView.delete_wish_list_button

    }

    interface OnDeleteWishListListener{
        fun deleteWishList(id:Int)
    }
}
