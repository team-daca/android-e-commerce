package com.doniabeje.android.ecommerce.data

import androidx.room.Entity
import androidx.room.PrimaryKey

//User.kt
@Entity
open class User(
    @PrimaryKey val id: Int = -1,
    val email: String?,
    val password: String?,
    val role: UserRole? = null,
    val userRoleId: Int,
    val name: String = ""
) {

    companion object {
        var loggedInUser: User? = null
    }
}