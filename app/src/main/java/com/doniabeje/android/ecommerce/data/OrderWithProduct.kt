package com.doniabeje.android.ecommerce.data

import androidx.room.Embedded
import androidx.room.Relation

//OrderWithProduct.kt
class OrderWithProduct(
    @Embedded val order: Order, @Relation(
        parentColumn = "id",
        entityColumn = "id"
    ) val product: Product
)