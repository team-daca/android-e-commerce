package com.doniabeje.android.ecommerce.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation


data class ProductWithReview(
    @Embedded val product: Product,
    @Relation(parentColumn = "id", entityColumn = "productId", entity = ProductReview::class )
    val reviews: List<ProductReview>

)