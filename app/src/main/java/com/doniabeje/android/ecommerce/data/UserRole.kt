package com.doniabeje.android.ecommerce.data

import androidx.room.Entity
import androidx.room.PrimaryKey

//UserRole.kt
@Entity
data class UserRole(
    @PrimaryKey val id: Int = -1,
    val name: String
)