package com.doniabeje.android.ecommerce.viewmodel

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.doniabeje.android.ecommerce.OrdersRecyclerViewAdapter
import com.doniabeje.android.ecommerce.data.Order
import com.doniabeje.android.ecommerce.data.Product
import com.doniabeje.android.ecommerce.data.User
import com.doniabeje.android.ecommerce.network.APIService
import com.doniabeje.android.ecommerce.repository.EcommerceRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class OrdersViewModel(application: Application) : BaseViewModel(application), OrdersRecyclerViewAdapter.OnDeleteOrderListener {
    override fun deleteOrder(id: Int) {
        if (!repository.isConnected()){
            repository.notConnected()
            return
        }
        viewModelScope.launch {
            val deleted = repository.deleteOrder(id)
            viewModelScope.launch(Dispatchers.Main) {
                if (deleted) {
                    Toast.makeText(getApplication(), "Order Deleted", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(getApplication(), "Unable To Deleted Order ", Toast.LENGTH_SHORT).show()

                }
            }

        }
    }

    lateinit var orders: LiveData<List<Order>>

    init {
        val userId = User.loggedInUser?.id
        userId?.let {
            when (User.loggedInUser?.role?.name) {

                "RETAILER" -> orders = repository.getRetailerOrders(it)
                else -> orders = repository.getCustomerOrders(it)
            }
        }

    }

    fun updateOrder(order: Order, amount:Int){
        if (!repository.isConnected()){
            repository.notConnected()
            return
        }
        order.amount = amount
        viewModelScope.launch {
            val updated = repository.updateOrder(order)
            viewModelScope.launch(Dispatchers.Main){
                if (updated){
                    Toast.makeText(getApplication(), "Order Updated", Toast.LENGTH_SHORT).show()
                }
                else {
                    Toast.makeText(getApplication(), "Unable To Updated Order ", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

}