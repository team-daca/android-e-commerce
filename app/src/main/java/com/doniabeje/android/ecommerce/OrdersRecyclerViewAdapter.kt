package com.doniabeje.android.ecommerce

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.doniabeje.android.ecommerce.data.Order
import kotlinx.android.synthetic.main.fragment_order_card.view.*


import kotlinx.android.synthetic.main.fragment_product.view.*

class OrdersRecyclerViewAdapter(
    private var orders: List<Order>,
    private var editOrderListener: OnEditOrderListener,
    private var deleteOrderListener: OnDeleteOrderListener
) : RecyclerView.Adapter<OrdersRecyclerViewAdapter.ViewHolder>() {


    fun updateOrders(orders: List<Order>) {
        this.orders = orders
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_order_card, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val order = orders[position]
        val price = order.product?.price.toString()
        val name = order.product?.name
        val amount = order.amount
        with(holder) {
            mName.text = name
            mPriceView.text = "$$price X  $amount"
            mEditOrderButton.setOnClickListener { editOrderListener.editOrder(order) }
            mDeleteOrderButton.setOnClickListener { deleteOrderListener.deleteOrder(order.id) }
        }

    }

    override fun getItemCount(): Int = orders.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mName: TextView = mView.orderProductName
        val mPriceView: TextView = mView.orderProductPrice
        val mEditOrderButton = mView.edit_order_button
        val mDeleteOrderButton = mView.delete_order_button

    }
    interface OnEditOrderListener{
        fun editOrder(order:Order)
    }
    interface OnDeleteOrderListener{
        fun deleteOrder(id:Int)
    }
}
