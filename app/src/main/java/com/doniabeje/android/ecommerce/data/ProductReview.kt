package com.doniabeje.android.ecommerce.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

//ProductReview.kt
@Entity
data class ProductReview(
    @PrimaryKey val id: Int = 0,
    val userId: Int,
    @Ignore
    val reviewer: User? = null,
    @Ignore
    val product: Product? = null,
    val productId: Int,
    @Ignore
    val message: String?,
    var rating: Int
) {
    constructor(
         id: Int=0,
         userId: Int,
         productId: Int,
         rating: Int
        ): this(id, userId, null, null, productId, null, rating)
}