package com.doniabeje.android.ecommerce.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

//WishList.kt
@Entity
data class WishList(
    @PrimaryKey val id: Int = 0,
    @Embedded(prefix = "product_")
    val product: Product? = null,
    val productId: Int,
    val customerId: Int,
    @Ignore
    val customer: Customer?
){
    constructor(
        id: Int = 0,
        product: Product?,
        productId: Int,
        customerId: Int
        ) : this(id, product, productId, customerId, null)
}