package com.doniabeje.android.ecommerce.repository

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import com.doniabeje.android.ecommerce.data.*
import com.doniabeje.android.ecommerce.network.APIService
import kotlinx.coroutines.*
import retrofit2.Response

class EcommerceRepository(private val context: Context, private val apiService: APIService, val dao: EcommerceDao) {


    fun login(user: User): Deferred<Response<User>> {

        return apiService.login(user)

    }

    fun getAllProducts(): LiveData<List<ProductWithReview>> {

        if (isConnected()) {
            GlobalScope.async(Dispatchers.IO) {
                val response: Response<List<Product>> = apiService.getProducts().await()
                if (response.isSuccessful) {
                    response.body()?.let {
                        dao.insertProduct(*it.toTypedArray())

                        it.forEach{e -> e.reviews?.forEach { f ->dao.insertProductReview(f) }}
                    }

                }


            }
        } else {
            notConnected()
        }
        return dao.getProductWithReviews()
    }

    suspend fun getProduct(id: Int): Product? {
        return dao.getProduct(id)
    }

    suspend fun getProductWithReview(id: Int): ProductWithReview? {
        return dao.getProductWithReview(id)
    }

    fun getRetailerProducts(id: Int): LiveData<List<Product>> {
        var products: List<Product>? = null
        if (isConnected()) {
            GlobalScope.async(Dispatchers.IO) {
                val response: Response<List<Product>> = apiService.getRetailerProducts(id).await()
                if (response.isSuccessful) {
                    response.body()?.let {
                        dao.insertProduct(*it.toTypedArray())
                    }
                }
            }
        } else {
            notConnected()
        }
        return dao.getAllProducts()
    }

    fun getRetailerOrders(id: Int): LiveData<List<Order>> {
        if (isConnected()) {
            GlobalScope.async(Dispatchers.IO) {
                val response: Response<List<Order>> = apiService.getRetailerOrders(id).await()
                if (response.isSuccessful) {
                    response.body()?.let {
                        dao.insertOrder(*it.toTypedArray())
                    }
                }
            }
        } else {
            notConnected()
        }
        return dao.getAllOrders()
    }

    fun getCustomerOrders(id: Int): LiveData<List<Order>> {
        if (isConnected()) {
            GlobalScope.async(Dispatchers.IO) {
                val response: Response<List<Order>> = apiService.getCustomerOrders(id).await()
                if (response.isSuccessful) {
                    response.body()?.let {
                        dao.insertOrder(*it.toTypedArray())
                    }
                }
            }
        } else {
            notConnected()
        }
        return dao.getAllOrders()
    }


    suspend fun addProduct(product: Product?): Boolean {


        val response: Response<Product> = apiService.addProduct(product).await()
        if (response.isSuccessful) {
            val product = response.body()
            product?.let {
                GlobalScope.launch(Dispatchers.IO) {
                    dao.insertProduct(it)
                }
            }
            return true
        }
        return false

    }

    suspend fun deleteProduct(id: Int): Boolean{
        val response: Response<Void> = apiService.deleteProduct(id).await()
        if (response.isSuccessful) {
                GlobalScope.launch(Dispatchers.IO) {
                    dao.deleteProduct(id)
                    dao.deleteOrderByProductID(id)
                    dao.deleteWishListByProductID(id)
                    dao.deleteProductReviewByProductID(id)
            }
            return true
        }
        return false
    }


    suspend fun updateProduct(product: Product): Boolean{
        val response: Response<Void> = apiService.updateProduct(product.id, product).await()
        if (response.isSuccessful) {
            GlobalScope.launch(Dispatchers.IO) {
                dao.insertProduct(product)

            }
            return true
        }
        return false
    }

    suspend fun updateOrder(order: Order): Boolean{
        val response: Response<Void> = apiService.updateOrder(order.id, order).await()
        if (response.isSuccessful) {
            GlobalScope.launch(Dispatchers.IO) {
                dao.insertOrder(order)

            }
            return true
        }
        return false
    }

    suspend fun deleteOrder(id: Int): Boolean{
        val response: Response<Void> = apiService.deleteOrder(id).await()
        if (response.isSuccessful) {
            GlobalScope.launch(Dispatchers.IO) {
                dao.deleteOrder(id)

            }
            return true
        }
        return false
    }

    suspend fun addOrder(order: Order): Boolean {
        val response: Response<Order> = apiService.addOrder(order).await()
        if (response.isSuccessful) {
            val order = response.body()
            order?.let {
                GlobalScope.launch(Dispatchers.IO) {
                    dao.insertOrder(it)
                }
            }
            return true
        }

        return false
    }

    //WishList
    suspend fun addWishList(wishList: WishList): Boolean {
        val response: Response<WishList> = apiService.addWishList(wishList).await()
        Log.d("asddf", response.message())
        if (response.isSuccessful) {
            val wishList = response.body()
            wishList?.let {
                GlobalScope.launch(Dispatchers.IO) {
                    dao.insertWishList(it)
                }
            }
            return true
        }

        return false
    }

    suspend fun deleteWishList(id: Int): Boolean{
        val response: Response<Void> = apiService.deleteWishList(id).await()
        if (response.isSuccessful) {
            GlobalScope.launch(Dispatchers.IO) {
                dao.deleteWishList(id)
            }
            return true
        }
        return false
    }

    fun getCustomerWishLists(id: Int): LiveData<List<WishList>> {
        if (isConnected()) {
            GlobalScope.async(Dispatchers.IO) {
                val response: Response<List<WishList>> = apiService.getCustomerWishLists(id).await()
                if (response.isSuccessful) {
                    response.body()?.let {
                        dao.insertWishList(*it.toTypedArray())
                    }
                }
            }
        } else {
            notConnected()
        }
        return dao.getAllWishLists()
    }

    //Product Review

    suspend fun addProductReview(productReview: ProductReview): Boolean {


        val response: Response<ProductReview> = apiService.addProductReview(productReview).await()
        if (response.isSuccessful) {
            val product = response.body()
            product?.let {
                GlobalScope.launch(Dispatchers.IO) {
                    dao.insertProductReview(it)
                }
            }
            return true
        }
        return false

    }

    suspend fun deleteProductReview(id: Int): Boolean{
        val response: Response<Void> = apiService.deleteProductReview(id).await()
        if (response.isSuccessful) {
            GlobalScope.launch(Dispatchers.IO) {
                dao.deleteProductReview(id)
            }
            return true
        }
        return false
    }


    suspend fun updateProductReview(productReview: ProductReview): Boolean{
        val response: Response<Void> = apiService.updateProductReviews(productReview.id, productReview).await()
        if (response.isSuccessful) {
            GlobalScope.launch(Dispatchers.IO) {
                dao.insertProductReview(productReview)

            }
            return true
        }
        return false
    }

    fun addUser(user: User): Deferred<Response<User>> {

        return apiService.addUser(user)

    }


    fun clearDatabase() {
        GlobalScope.async {
            dao.deleteAllProduct()
            dao.deleteAllOrder()
            dao.deleteAllWishList()
            dao.deleteAllProductReview()
        }
    }


    fun notConnected() {

        Toast.makeText(context, "Not Connected", Toast.LENGTH_SHORT).show()
    }

    private fun requestFailed() {

        Toast.makeText(context, "Request Faild", Toast.LENGTH_SHORT).show()
    }

    fun isConnected(): Boolean {

        val connectivityManager = context.getSystemService(CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }


}