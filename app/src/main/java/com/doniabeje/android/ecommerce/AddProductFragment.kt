package com.doniabeje.android.ecommerce

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.doniabeje.android.ecommerce.data.Product
import com.doniabeje.android.ecommerce.databinding.AddProductFragmentBinding
import com.doniabeje.android.ecommerce.viewmodel.AddProductViewModel


class AddProductFragment : Fragment() {
    lateinit var binding: AddProductFragmentBinding
    lateinit var viewModel: AddProductViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(AddProductViewModel::class.java)

        binding = DataBindingUtil.inflate(inflater, R.layout.add_product_fragment, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }


}
