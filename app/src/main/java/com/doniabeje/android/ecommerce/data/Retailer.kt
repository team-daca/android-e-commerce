package com.doniabeje.android.ecommerce.data

import androidx.room.Entity

//Retailer.kt
@Entity
class Retailer(
    val products: List<Product>,
    val orders: List<Order>,
    id: Int = -1,
    email: String,
    password: String,
    role: UserRole,
    userRoleId: Int,
    name: String
) : User(id, email, password, role, userRoleId, name)