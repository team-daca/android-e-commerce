package com.doniabeje.android.ecommerce.viewmodel

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.viewModelScope
import com.doniabeje.android.ecommerce.data.*
import com.doniabeje.android.ecommerce.network.APIService
import com.doniabeje.android.ecommerce.repository.EcommerceRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

//SingleProductViewModel.kt
class SingleProductViewModel(application: Application) : BaseViewModel(application) {
    var product: MutableLiveData<Product> = MutableLiveData()
    var productWithReview: MutableLiveData<ProductWithReview> = MutableLiveData()


    fun addReview(productReview: ProductReview) {

    }

    fun addOrder(amount: Int) {
        productWithReview.value?.let {
            val loggedInUser = User.loggedInUser
            val product = it.product
            loggedInUser?.let {
                val order =
                    Order(amount = amount, productId = product.id, retailerId = product.retailerId, customerId = it.id)
                if (repository.isConnected()){

                    viewModelScope.launch {
                        val added = repository.addOrder(order)
                        viewModelScope.launch(Dispatchers.Main) {
                            Toast.makeText(getApplication(), "Orderd", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
                else{
                    repository.notConnected()
                }

            }

        }
    }

    fun updateRating(rating:Float, review: ProductReview){

                if (repository.isConnected()){

                    review.rating = rating.toInt()
                    viewModelScope.launch {
                        val updated = repository.updateProductReview(review)
                        viewModelScope.launch(Dispatchers.Main) {
                            if (updated){
                                Toast.makeText(getApplication(), "updated", Toast.LENGTH_SHORT).show()

                            }
                        }
                    }
                }
                else{
                    repository.notConnected()
                }


    }

    fun addRating(rating:Float){
        productWithReview.value?.let {
            val loggedInUser = User.loggedInUser
            val product = it.product
            loggedInUser?.let {
                val review =
                    ProductReview(productId = product.id, userId = it.id, rating = rating.toInt())
                if (repository.isConnected()){

                    viewModelScope.launch {
                        val updated = repository.addProductReview(review)
                        viewModelScope.launch(Dispatchers.Main) {
                            if (updated){
                                Toast.makeText(getApplication(), "added", Toast.LENGTH_SHORT).show()

                            }
                        }
                    }
                }
                else{
                    repository.notConnected()
                }

            }

        }
    }
    fun addOrUpdateRating(rating: Float){
        val loggedInUser = User.loggedInUser
        val review = productWithReview.value?.reviews?.find { e -> e.userId == loggedInUser?.id }
        if (review == null){
            //add new
            addRating(rating)
        }
        else{
            //update
            updateRating(rating, review)
        }
    }
    fun getProduct(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getProduct(id)?.let {
                product.postValue(it)
            }
        }
    }
    fun getProductWithReview(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getProductWithReview(id)?.let {
                productWithReview.postValue(it)
            }
        }
    }





}
