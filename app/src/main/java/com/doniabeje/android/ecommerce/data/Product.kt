package com.doniabeje.android.ecommerce.data

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

//Product.kt
@Entity
data class Product(
    @PrimaryKey val id: Int = 0,
    var name: String?,
    var price: Float?,
    val image: String?,
    val retailerId: Int,
    @Ignore
    val retailer: Retailer? = null,
    @Ignore
    val reviews: List<ProductReview>? = null
) {

    constructor(
        id: Int = 0,
        name: String?,
        price: Float?,
        image: String?,
        retailerId: Int
    ) : this(id, name, price, image, retailerId, null, null)
}