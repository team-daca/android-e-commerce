package com.doniabeje.android.ecommerce.data

import androidx.room.Entity
import java.io.Serializable

//Customer.kt
@Entity
class Customer(
    id: Int = -1,
    email: String,
    password: String,
    role: UserRole,
    userRoleId: Int,
    name: String,
    val orders: List<Order>,
    val productReviews: List<ProductReview>,
    val wishLists: List<WishList>
) : User(id, email, password, role, userRoleId, name), Serializable