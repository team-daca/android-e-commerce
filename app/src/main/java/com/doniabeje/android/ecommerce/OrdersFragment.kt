package com.doniabeje.android.ecommerce

import android.app.AlertDialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.doniabeje.android.ecommerce.data.Order
import com.doniabeje.android.ecommerce.data.Product
import com.doniabeje.android.ecommerce.data.User
import com.doniabeje.android.ecommerce.databinding.FragmentOrdersBinding
import com.doniabeje.android.ecommerce.viewmodel.OrdersViewModel
import kotlinx.android.synthetic.main.edit_order.view.*
import kotlinx.android.synthetic.main.edit_product.view.*


class OrdersFragment : Fragment(), OrdersRecyclerViewAdapter.OnEditOrderListener {
    override fun editOrder(order: Order) {
        showEditOrderDialog(order)
    }

    lateinit var ordersRecyclerViewAdapter: OrdersRecyclerViewAdapter
    lateinit var viewModel: OrdersViewModel
    lateinit var binding: FragmentOrdersBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(OrdersViewModel::class.java)
        ordersRecyclerViewAdapter = OrdersRecyclerViewAdapter(listOf(), this, viewModel)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_orders, container, false)
        with(binding.ordersRecyclerView) {
            adapter = ordersRecyclerViewAdapter
            layoutManager = LinearLayoutManager(context)

        }

        viewModel.orders.observe(
            viewLifecycleOwner,
            Observer { orders ->
                ordersRecyclerViewAdapter.updateOrders(orders)
            })

        return binding.root
    }


    fun showEditOrderDialog(order: Order) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Edit Order")
        val editView = LayoutInflater.from(context).inflate(R.layout.edit_order, null)
        editView.edit_ordere_amount_edit_text.setText(order.amount.toString())
        builder.setView(editView)
        builder.setNegativeButton(android.R.string.cancel) { dialog, p1 ->
            dialog.cancel()
        }
        builder.setPositiveButton(android.R.string.ok) { dialog, p1 ->
            viewModel.updateOrder(
                order,
                editView.edit_ordere_amount_edit_text.text.toString().toInt()
            )
        }
        builder.show()
    }
}
