package com.doniabeje.android.ecommerce


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.doniabeje.android.ecommerce.data.Product
import kotlinx.android.synthetic.main.fragment_my_product.view.*
import kotlinx.android.synthetic.main.fragment_order_card.view.*
import kotlinx.android.synthetic.main.fragment_product.view.myProductName
import kotlinx.android.synthetic.main.fragment_product.view.myProductPrice

class MyProductRecyclerViewAdapter(
    private var products: List<Product>,
    private var deleteProductListener: OnDeleteProductListener,
    private var editProductListener: OnEditProductListener
) : RecyclerView.Adapter<MyProductRecyclerViewAdapter.ViewHolder>() {


    fun updateProducts(products: List<Product>) {
        this.products = products
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_my_product, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = products[position]
        val price = product.price.toString()
        val name = product.name
        with(holder) {
            mName.text = name
            mPriceView.text = "$$price"
            deleteButton.setOnClickListener { deleteProductListener?.deleteProduct(product.id)  }
            editButton.setOnClickListener { editProductListener?.editProduct(product) }
        }


    }

    override fun getItemCount(): Int = products.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mName: TextView = mView.myProductName
        val mPriceView: TextView = mView.myProductPrice
        val deleteButton : ImageButton = mView.delete_Product_button
        val editButton: ImageButton = mView.edit_order_button_my_product


    }

    interface OnDeleteProductListener{
        fun deleteProduct(id:Int)
    }
    interface OnEditProductListener{
        fun editProduct(products: Product)
    }
}
