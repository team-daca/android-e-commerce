package com.doniabeje.android.ecommerce.viewmodel

import android.app.Application
import android.content.SharedPreferences
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.Navigation
import com.doniabeje.android.ecommerce.R
import com.doniabeje.android.ecommerce.data.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FeedViewModel(application: Application) : BaseViewModel(application) {
    var products: LiveData<List<ProductWithReview>> = repository.getAllProducts()
    var wishList: LiveData<List<WishList>> = MutableLiveData<List<WishList>>(listOf())
    var sharedPreferences: SharedPreferences? = null
    fun navToOrdersFragment(view: View) {
        Navigation.findNavController(view).navigate(R.id.action_feedFragment_to_ordersFragment)
    }

    fun navWishListFragment(view: View) {
        Navigation.findNavController(view).navigate(R.id.action_feedFragment_to_wishListFragment)
    }

    fun getCustomerWishLists(id: Int) {
        wishList = repository.getCustomerWishLists(id)
    }

    fun addWishList(product: Product) {
        val loggedInUser = User.loggedInUser
        loggedInUser?.let {
            val wishList =
                WishList(productId = product.id, product = null, customerId = it.id)
            if (repository.isConnected()) {

                viewModelScope.launch {
                    val added = repository.addWishList(wishList)
                    viewModelScope.launch(Dispatchers.Main) {
                        if (added) {
                            Toast.makeText(getApplication(), "Added To Wish List", Toast.LENGTH_SHORT).show()

                        } else {
                            Toast.makeText(getApplication(), "Unable Add To Wish List", Toast.LENGTH_SHORT).show()

                        }
                    }
                }
            } else {
                repository.notConnected()
            }

        }


    }

    fun logout(view: View) {
        sharedPreferences?.let {
            with(it.edit()) {
                remove(com.doniabeje.android.ecommerce.viewmodel.USERID)
                putString(com.doniabeje.android.ecommerce.viewmodel.USERROLE, "")
                commit()
            }
        }

        repository.clearDatabase()
        Navigation.findNavController(view)
            .popBackStack()


    }

    fun deleteWishList(id: Int) {
        if (!repository.isConnected()) {
            repository.notConnected()
            return
        }
        viewModelScope.launch {
            val deleted = repository.deleteWishList(id)
            viewModelScope.launch(Dispatchers.Main) {
                if (deleted) {
                    Toast.makeText(getApplication(), "Wish List Deleted", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(getApplication(), "Unable To Deleted Wish List ", Toast.LENGTH_SHORT).show()

                }
            }

        }
    }


}
