package com.doniabeje.android.ecommerce.viewmodel

import android.app.Application
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.doniabeje.android.ecommerce.R
import com.doniabeje.android.ecommerce.data.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import retrofit2.Response

//RegisterViewModel.kt
class RegisterViewModel(application: Application) : BaseViewModel(application) {

    var errorMessage = MutableLiveData<String>("nothing")
    var email = ObservableField<String>("")
    var password = ObservableField<String>("")
    var isRetailer = ObservableField<Boolean>(true)

    fun register(view: View) {

        errorMessage.postValue("Loading...")
        var user = User(email = email.get(), password = password.get(), userRoleId = 1)
        isRetailer?.get()?.let {
            if (it) {
                user = User(email = email.get(), password = password.get(), userRoleId = 2)
            }
        }

        if (repository.isConnected()) {

            viewModelScope.launch(Dispatchers.IO) {
                val response: Response<User> = repository.addUser(user).await()
                if (response.isSuccessful) {
                    errorMessage.postValue("Registered")
                }
                //BadRequest
                else if (response.code() == 400) {
                    errorMessage.postValue("Email is Already InUse")
                } else {
                    errorMessage.postValue("Error! Try again")

                }
            }


        } else {
            repository.notConnected()

        }
    }


}


