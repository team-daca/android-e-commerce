package com.doniabeje.android.ecommerce.viewmodel

import android.app.Application
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.viewModelScope
import com.doniabeje.android.ecommerce.data.Product
import com.doniabeje.android.ecommerce.data.User
import com.doniabeje.android.ecommerce.network.APIService
import com.doniabeje.android.ecommerce.repository.EcommerceRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AddProductViewModel(application: Application) : BaseViewModel(application) {

    var name = ObservableField<String>("")
    var price = ObservableField<Float>(0F)


    fun addProduct() {
        if (repository.isConnected()){

            User.loggedInUser?.let {
                val product = Product(name = name.get(), price = price.get(), image = "", retailerId = it.id)
                viewModelScope.launch {
                    val added = repository.addProduct(product)
                    if (added){
                        viewModelScope.launch(Dispatchers.Main){
                            Toast.makeText(getApplication(), "Product Added", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
        else{
            repository.notConnected()
        }



    }
}
