package com.doniabeje.android.ecommerce

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.doniabeje.android.ecommerce.data.Product
import android.view.View
import androidx.navigation.Navigation
import com.doniabeje.android.ecommerce.data.ProductReview
import com.doniabeje.android.ecommerce.data.ProductWithReview
import com.doniabeje.android.ecommerce.data.WishList
import kotlinx.android.synthetic.main.fragment_product.view.*
import java.lang.Exception

class FeedRecyclerViewAdapter(private var productWithReviews: List<ProductWithReview>, private  var wishList: List<WishList>, private var wishListToggleListener:OnWishListToggleListener) :
    RecyclerView.Adapter<FeedRecyclerViewAdapter.ViewHolder>() {

    fun updateProductWithReviewss(productWithReviews: List<ProductWithReview>) {
        this.productWithReviews = productWithReviews
        notifyDataSetChanged()
    }
    fun updateWishList(wishList: List<WishList>) {
        this.wishList = wishList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_product, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = productWithReviews.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = productWithReviews[position].product
        val review = productWithReviews[position].reviews
        holder.productNameTextView.text = product.name
        holder.mRatingBar.rating = getAverageRating(review).toFloat()
        val price = product.price
        holder.productPriceTextView.text = "$$price"
        val mWishList = wishList.find { e -> e.productId == product.id }
        val inWishList  = mWishList != null
        if (inWishList){
            holder.mWishlistToggleButton.setImageResource(R.drawable.ic_favorite_primary_24dp)
        }
        else{
            holder.mWishlistToggleButton.setImageResource(R.drawable.ic_favorite_border_dark_blue_24dp)
        }

        holder.view.setOnClickListener {
            val args = Bundle()
            args.putInt("ProductId", product.id)
            val navigation = Navigation.findNavController(it)
            navigation.navigate(R.id.action_feedFragment_to_singleProductFragment, args)
        }
        holder.mWishlistToggleButton.setOnClickListener {
            wishListToggleListener.toggle(mWishList, product)
        }
    }


    fun getAverageRating(reviews:List<ProductReview>): Int{
        var rating = 0

        try{
            reviews?.forEach { e -> rating += e.rating }
            rating /= reviews?.size
        }catch (e: Exception){}

        return rating
    }
    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val productNameTextView = view.myProductName
        val productPriceTextView = view.myProductPrice
        val mRatingBar = view.ratingBar
        val mWishlistToggleButton = view.toggle_wishlist_button
    }

    interface OnWishListToggleListener{
        fun toggle(wishList: WishList?, product: Product)
    }


}