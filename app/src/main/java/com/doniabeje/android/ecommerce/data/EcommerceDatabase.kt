package com.doniabeje.android.ecommerce.data


import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


//EcommerceDatabase.kt
@Database(
    entities = arrayOf(
        Product::class,
        Order::class,
        WishList::class,
        ProductReview::class
    ), version = 1
)
abstract class EcommerceDatabase : RoomDatabase() {
    abstract fun ecommerceDao(): EcommerceDao

    companion object {
        @Volatile
        private var INSTANCE: EcommerceDatabase? = null

        fun getDatabase(context: Context): EcommerceDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    EcommerceDatabase::class.java,
                    "ecommerce_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}